﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Home.aspx.vb" Inherits="WebAppEx.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="~/Content/Home/css/main.css" >
    <title>Home | Capacitacion .Net F</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-light bg-light col-12 shadow-sm">
              <a class="navbar-brand" href="/Home">
                <img src="<%=ResolveClientUrl("~/Content/Home/img/logoSASF.png") %>" width="100"class="d-inline-block align-top" alt="">
              </a>
              <div class="font-weight-light">Capacitación perfil DotNet</div>
            </nav>
        </div>
        <div class="row justify-content-center">
            <div class="col-4 mt-4 shadow-sm p-4 bg-white">
                <div ID="confirmacionRegistro" runat="server" class="alert alert-success" Visible="False">Datos registrados exitosamente</div>
                <form id="form1" runat="server">
                    <div class="form-group">
                        <asp:Label ID="nombreLabel" runat="server" Text="Nombre"></asp:Label>
                        <asp:TextBox ID="nombreInput" class="form-control" runat="server"></asp:TextBox>
                        <small class="form-text text-muted">El nombre del usuario</small>
                         <div ID="validacionUsuario" runat="server" class="validation-box font-weight-light alert alert-danger" Visible="False">Debe otorgar un nombre de usuario con menos de 15 caracteres.</div>

                    </div>
                    <div class="form-group">
                        <asp:Label ID="edadLabel" runat="server" Text="Edad"></asp:Label>
                        <asp:TextBox ID="edadInput" class="form-control" runat="server"></asp:TextBox>
                        <small class="form-text text-muted">La edad del usuario</small>
                        <div ID="validacionEdad" runat="server" class="validation-box font-weight-light alert alert-danger" Visible="False">Debe otorgar una edad para el usuario mayor a 18 y menor que 45.</div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="correoLabel" runat="server" Text="Correo"></asp:Label>
                        <asp:TextBox ID="correoInput" class="form-control" runat="server"></asp:TextBox>
                        <small class="form-text text-muted">El correo del usuario</small>
                        <div ID="validacionCorreo" runat="server" class="validation-box font-weight-light alert alert-danger" Visible="False">Debe proporcionar un correo válido.</div>
                    </div>
                    <div class="row justify-content-end mr-0">
                        <asp:Button ID="btnEnviar" class="btn btn-primary mr-2" runat="server" Text="Registrar datos" OnClick="BtnRegistrarDatos"/>
                        <button id="btnCancel" type="button" class="btn btn-danger">Cancelar envio</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="<%=ResolveClientUrl("~/Content/Home/js/main.js")%>">
    </script>
</body>
</html>
