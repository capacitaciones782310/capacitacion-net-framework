﻿Public Class Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        validacionUsuario.Visible = False
        validacionEdad.Visible = False
        validacionCorreo.Visible = False
        confirmacionRegistro.Visible = False

    End Sub

    Protected Sub BtnRegistrarDatos(sender As Object, e As EventArgs)

        Dim NombreUsuario As String = nombreInput.Text
        Dim EdadUsuario As String = edadInput.Text
        Dim CorreoUsuario As String = correoInput.Text

        If ValidarFormularioUsuario(NombreUsuario, EdadUsuario, CorreoUsuario) Then

            LimpiarFormularioUsuario()
            confirmacionRegistro.Visible = True

        End If

    End Sub

    Private Function ValidarFormularioUsuario(
        nombre As String, Edad As String, Correo As String) As Boolean

        Dim FormularioValido As Boolean = True

        If (nombre.Length > 15) OrElse (nombre = String.Empty) Then

            validacionUsuario.Visible = True
            FormularioValido = False

        End If

        If (Edad = String.Empty OrElse
            Not IsNumeric(Edad) OrElse
            Not ((Convert.ToInt32(Edad) > 18) And
            (Convert.ToInt32(Edad) < 45))) Then

            validacionEdad.Visible = True
            FormularioValido = False

        End If

        Dim correoRegex As New Regex("^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$")
        If Not correoRegex.IsMatch(Correo) OrElse (Correo = String.Empty) Then

            validacionCorreo.Visible = True
            FormularioValido = False

        End If

        Return FormularioValido

    End Function

    Private Sub LimpiarFormularioUsuario()

        nombreInput.Text = ""
        edadInput.Text = ""
        correoInput.Text = ""

    End Sub

End Class
