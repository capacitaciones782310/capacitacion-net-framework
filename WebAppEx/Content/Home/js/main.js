﻿
let btnCancel = document.querySelector("#btnCancel"),
    formBoxes = document.querySelectorAll(".form-control");

btnCancel.addEventListener("click", (e) =>
    formBoxes.forEach(inputText => inputText.value = "") );
