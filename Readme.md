
# **Capacitacion .Net Framework 4**

## **Sintaxis VB.Net**
- 1.1 Estructura de una solución y proyectos en Visual Studio
- 1.2 Atajos de teclado y herramientas de depuración
- 1.3 Declaraciones, variables y literales
- 1.4 Estructuras de control de flujo
- 1.5 Arrays y colecciones de datos
- 1.6 LINQ
- 1.7 Ejercicio: Manipulación de listas

## **ASP.NET**
- 2.1 Estructura del proyecto
- 2.2 Modelos y formularios web
- 2.3 Manejadores de eventos para formularios web
- 2.4 Ejercicio: Página con formulario de datos

## **Recursos adicionales**
- LINQ Lista de Operadores
- Referencia del lenguaje VisualBasic
- Documentación Bootstrap

## **Sesiones grabadas**
