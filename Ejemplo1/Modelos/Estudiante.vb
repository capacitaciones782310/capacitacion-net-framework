﻿
Public Class Estudiante : Inherits Persona

    Private _Curso As String

    Public Property Curso As String

        Get
            Return _Curso
        End Get

        Set(curso As String)
            _Curso = curso
        End Set

    End Property

    Public Sub New(Optional cursop As String = ".Net Framework")

        MyBase.New()
        Console.WriteLine("Inicializando constructor estudiante")
        Curso = cursop

    End Sub

    Public Function Getmessage() As String

        Return "Hola desde la clase estudiante"

    End Function

    Public Sub GetAlgo()
    End Sub

End Class
