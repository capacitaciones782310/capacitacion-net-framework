﻿
Module Module1

    Sub Main()

#Region "Declaraciones tipos y literales"

        Console.WriteLine("Declaraciones tipos y literales")
        Dim NumeroTMP As Int64 = 12
        Dim NumeroPasantes As String = "12"
        Dim NumeroPConversion As Integer = Convert.ToInt32(NumeroPasantes)
        Dim NombrePasante As String = "Alan"
        Const PI As Decimal = 3.14159265
#End Region

#Region "Estructuras de control de flujo"

        Console.WriteLine("Estructuras de control de flujo")
        If (NumeroTMP = NumeroPConversion) And Convert.ToInt32(NumeroPasantes) = 12 Then
            Console.WriteLine("Números correctos")
        Else
            Console.WriteLine("Números incorrectos")
        End If

        Dim Centinela As Short = 1

        While Centinela = 0

            ' Conversión de tipos representativos
            Console.WriteLine("Ingrese el número 1 para continuar o 0 para salir: ")

            ' Si se convierte un tipo de dato que no tiene una representación
            ' para el tipo de dato destino, el programa lanza una excepción
            Centinela = Convert.ToInt32(Console.ReadLine())

        End While
#End Region

#Region "Arrays y colecciones de datos"

        Console.WriteLine("Arrays y colecciones de datos")
        Dim NombresAsistentes() As String = {
                "Luís Sarmiento",
                "Gisell Villarreal",
                "Daniel Sanchez",
                "Freya Lopez"
            }

        Dim ListaNumeros As List(Of Integer) = New List(Of Integer)
        For X = 0 To 10
            ListaNumeros.Add(X + 1)
        Next

        Dim ListaAsistentes As List(Of Persona) = New List(Of Persona) From {
                New Estudiante With {
                    .Nombre = "Freya Lopez",
                    .Edad = 20,
                    .Cedula = "09XXXXXXX"
                },
                New Estudiante With {
                    .Nombre = "Francisco Mayancela",
                    .Edad = 22,
                    .Cedula = "09XXXXXXX"
                },
                New Estudiante With {
                    .Nombre = "Maria Rivera",
                    .Edad = 21,
                    .Cedula = "09XXXXXXX"
                },
                New Estudiante With {
                    .Nombre = "Joel Olvera",
                    .Edad = 21,
                    .Cedula = "09XXXXXXX"
                }
            }

        ListaAsistentes.Add(New Estudiante("Capacitacion .Net") With {
                .Nombre = "Joel Olvera",
                .Edad = 19,
                .Cedula = "09XXXXXXX"
            })

        For Each Numero In ListaNumeros
            Console.WriteLine(Numero)
        Next

        ListaAsistentes.ForEach(Sub(asistente) Console.WriteLine(asistente))

        Dim Matriculas As New Dictionary(Of String, String)

        Matriculas.Add("Alan", "20161105")
        Matriculas.Add("Julia", "20161105")
        Matriculas.Add("Jake", "20161105")
        Matriculas.Add("Maria", "20161105")

        If Matriculas.TryGetValue("Alan", "") Then
            Console.WriteLine(
            String.Format("La matricula de Alan es: {0}", Matriculas("Alan")))
        End If

        For Each Estudiante In Matriculas.Keys
            Console.WriteLine(
                String.Format("{0}: {1}", Estudiante, Matriculas(Estudiante)))
        Next

#End Region

#Region "Manipulación de fechas"

        Console.WriteLine("Manipulación de fechas:")
        Dim FechaActual As Date = Date.Now

        Console.WriteLine(
            String.Format(
                "Estamos en el periodo {1} y el dia de la semana es {2}",
                FechaActual.Month,
                FechaActual.Year,
                FechaActual.DayOfWeek
           )
        )

        Console.WriteLine(
                Date.Now.CompareTo(Date.Parse("01/12/2022").Date))

        Console.WriteLine(
                Date.Now.Date.CompareTo(Date.Parse("11/03/2023").Date))

        Console.WriteLine(
                Date.Now.CompareTo(Date.Parse("01/03/2024").Date))
#End Region

#Region "Manipulación de errores"

        Try

            Dim resultado As Boolean = Convert.ToBoolean("1")

        Catch ex As Exception

            Console.WriteLine("No se convertir un número a boleano")

        Finally

            Console.WriteLine("Excepciones controladas adecuadamente")

        End Try

#End Region

#Region "Manipulación de colecciones con LINQ"
        ' Operados básicos
        Dim ListaNotas As List(Of Integer) = New List(Of Integer) From {
            45, 66, 60, 94, 72, 44, 88, 20}

        'Operadores de agregación
        ' Min y Max
        Dim EdadEstudianteMasJoven = ListaAsistentes.Min(
            Function(estudiante) estudiante.Edad)

        Console.WriteLine(
            String.Format(
                "El estudiante más joven tiene {0} años.",
                EdadEstudianteMasJoven
        ))

        'Average y Aggregate
        Dim SumaTotal As Integer = ListaNotas.Aggregate(Function(a, b) a + b)
        Dim Promedio As Integer = ListaNotas.Average(Function(a) a)

#End Region
    End Sub

End Module
